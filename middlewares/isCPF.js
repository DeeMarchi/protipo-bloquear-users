const httpError = require('http-errors');

module.exports = (req, res, next) => {
    const regexCPF = /^\d{3}\.\d{3}\.\d{3}\-\d{2}$/;
    if (!regexCPF.test(req.query.usuario)) {
        next(httpError(422, 'Por gentileza, envie o CPF no formato correto! xxx-yyy-zzz.ww'));
    } else {
        next();
    }
};
