const httpError = require('http-errors');

module.exports = (req, res, next) => {
    if (!req.query?.usuario) {
        next(httpError(403, 'É necessário um usuário para prosseguirmos!'));
    } else {
        next();
    }
};
