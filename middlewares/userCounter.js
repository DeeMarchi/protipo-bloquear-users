const { VisitasUsers } = require('../models');
const { Blacklist } = require('../models');

module.exports = async (req, res, next) => {
    try {
        if (! await Blacklist.findOne({ where: { cpf: req.query.usuario }})) {
            const user = await VisitasUsers.findOrCreate({ where: { cpf: req.query.usuario }});
            await user[0].increment('num_visitas');
            req.contagem = user[0].num_visitas;
        }
        next();
    } catch (err) {
        next(err);        
    }
};
