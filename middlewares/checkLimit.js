const { Blacklist } = require('../models');
const httpError = require('http-errors');

module.exports = async (req, res, next) => {
    try {
        if (req.contagem >= 100) {
            const blacklistedUser = await Blacklist.findOne({ where: { cpf: req.query.usuario }});
            if (!blacklistedUser) {
                await Blacklist.create({ cpf: req.query.usuario });
            }
            next(httpError(401, 'Seu usuário está bloqueado!!'));
        }
        next();
    } catch (err) {
        next(err);
    }
};
