const app = require('../app');
const supertest = require('supertest');
const request = supertest(app);

describe('Testes de CPFs malformados', () => {
    it('Checks for malformed CPF', done => {
        request.get('/?usuario=205.713.66040')
            .expect(422, done);
    });
    
    it('Checks characters in CPF', done => {
        request.get('/?usuario=205.713.66a-40')
            .expect(422, done);
    });
});
