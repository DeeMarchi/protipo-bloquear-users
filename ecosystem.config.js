module.exports = {
  apps : [{
    name: 'BLOCK_USERS',
    script: './bin/www',
    watch: true,
    ignore_watch: [ 'node_modules' ],
    node_args: '-r dotenv/config'
  }, {
    name: 'MSG_CRON',
    script: './crons/messageEverySecond.js',
    watch: true,
    ignore_watch: [ 'node_modules' ]
  }],
};
