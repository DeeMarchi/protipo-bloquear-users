const express = require('express');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const errors = require('./errorHandling/middlewares');

const indexRouter = require('./routes/index');
const hasUser = require('./middlewares/hasUser');
const isCPF = require('./middlewares/isCPF');
const userCounter = require('./middlewares/userCounter');
const checkLimit = require('./middlewares/checkLimit');

const app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use(hasUser);
app.use(isCPF);
app.use(userCounter);
app.use(checkLimit);

app.use('/', indexRouter);

app.use(errors.notFound);
app.use(errors.printStackTrace);
app.use(errors.catchAll);

module.exports = app;
