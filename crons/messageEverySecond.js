const cron = require('node-cron');

const { VisitasUsers } = require('../models');
const Sequelize = require('sequelize');

cron.schedule('* * * * *', () => {
    VisitasUsers.update({
        num_visitas: 0
    }, { where: {
        num_visitas: { [Sequelize.Op.gte]: 0 }
    }});
});
