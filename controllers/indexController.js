module.exports = {

    getEndpoint1: async (req, res, next) => {
        try {
            res.json({ message: 'Hello world '});
        } catch (err) {
            next(err);
        }
    },
    
    getEndpoint2: async (req, res, next) => {
        try {
            res.json({ message: 'Endpoint 2' });
        } catch (err) {
            next(err);
        }
    }
};
