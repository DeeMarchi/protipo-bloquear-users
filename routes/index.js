const express = require('express');
const router = express.Router();

const indexController = require('../controllers/indexController');

/* GET home page. */
router.get('/', indexController.getEndpoint1);
router.get('/testando', indexController.getEndpoint2);

module.exports = router;
